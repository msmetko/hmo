from common import CLUB_LIMIT, FIELD_NAMES, POSITIONS, Player, LIMIT, top11, get_players
import argparse
from collections import Counter


def greedy(players):
    squad = []
    TOTAL_MONEY = 100.0
    club_counter = Counter()
    for position in POSITIONS:
        players_on_position = list(
            filter(lambda player: player.position == position, players)
        )
        num_best = LIMIT[position] // 2
        for _ in range(num_best):
            condition = True
            while condition:
                best_player = max(
                    players_on_position,
                    key=lambda player: (player.points, -player.price),
                )
                players_on_position.remove(best_player)
                condition = (
                    club_counter[best_player.club] >= 3 and best_player not in squad
                )
            club_counter.update([best_player.club])
            TOTAL_MONEY -= best_player.price
            squad.append(best_player)

    avg_price = TOTAL_MONEY / (15 - len(squad))
    for position in POSITIONS:
        players_on_position = list(
            filter(
                lambda player: player.position == position
                and player.price < avg_price
                and player not in squad,
                players,
            )
        )
        num_best = (LIMIT[position] + 1) // 2
        for _ in range(num_best):
            condition = True
            while condition:
                best_player = max(
                    players_on_position,
                    key=lambda player: (player.points, -player.price),
                )
                players_on_position.remove(best_player)
                condition = (
                    club_counter[best_player.club] >= 3 and best_player not in squad
                )
            club_counter.update([best_player.club])
            TOTAL_MONEY -= best_player.price
            squad.append(best_player)
    return squad


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--instance", required=True, type=argparse.FileType("r"))
    parser.add_argument("-o", "--output", required=True, type=argparse.FileType("w"))
    args = parser.parse_args()
    players = get_players(args.instance)
    squad = greedy(players)
    first_team, subs = top11(squad)
    args.output.write(",".join(map(lambda player: str(player.ID), first_team)) + "\n")
    args.output.write(",".join(map(lambda player: str(player.ID), subs)))
    return


if __name__ == "__main__":
    main()

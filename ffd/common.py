import csv
import dataclasses

CLUB_LIMIT = 3
FIELD_NAMES = ["ID", "position", "name", "club", "points", "price"]
POSITIONS = ["GK", "DEF", "MID", "FW"]
Player = dataclasses.make_dataclass(
    "Player",
    fields=zip(FIELD_NAMES, [int, str, str, str, float, float]),
    unsafe_hash=True,
)
Player.__eq__ = lambda self, o: self.ID == o.ID
LIMIT = dict(zip(POSITIONS, [2, 5, 5, 3]))
PLAYER_MINS = {"DEF": 3, "ATT": 1}


def get_players(instances_file):
    def process(player):
        player.ID = int(player.ID)
        player.points = float(player.points)
        player.price = float(player.price)
        return player

    reader = csv.DictReader(instances_file, fieldnames=FIELD_NAMES)
    return [process(Player(**row)) for row in reader]


def top11(squad):
    assert len(squad) == 15
    first_team = []
    subs = []
    squad = set(squad)
    # Handle GK differently
    for position in POSITIONS:
        players_on_position = set(
            filter(lambda player: player.position == position, squad)
        )
        if position == "GK":
            better_gk = max(players_on_position, key=lambda gk: gk.points)
            worse_gk = min(players_on_position, key=lambda gk: gk.points)
            first_team.append(better_gk)
            subs.append(worse_gk)
            squad -= set(players_on_position)
        elif position in PLAYER_MINS:
            field_players = sorted(
                players_on_position, key=lambda player: player.points, reverse=True
            )
            num_best = PLAYER_MINS[position]
            best_for_position = field_players[:num_best]
            first_team.extend(best_for_position)
            squad -= set(best_for_position)
    free_places = 11 - len(first_team)
    remaining_squad = sorted(squad, key=lambda player: player.points, reverse=True)
    first_team.extend(remaining_squad[:free_places])
    subs.extend(remaining_squad[free_places:])
    assert len(first_team) == 11
    assert len(subs) == 4
    return first_team, subs

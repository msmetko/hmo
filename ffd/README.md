# Fantasy football draft

Midterm task for the HMO course
[Task definition](https://www.fer.unizg.hr/_download/repository/HOM_2020_Fantasy_Football_Draft_Problem.pdf)


## Greedy search
Running the greedy search:

```bash
python3 greedy.py -i ${INSTANCE} -o ${OUT_FILE}
```

`${INSTANCE}` can be one of `instance/instances{1,2}.csv`. The outputs are stored in the `out/` directory.

## Improving the greedy solution with the local search
Running the local search:
```bash
python3 local.py -s ${SOLUTION} -i ${INSTANCE} -o ${OUT_FILE}
```
`${SOLUTION}` is the out file of the greedy search.

## Validation of the solutions

Run `python3 validator/validator.py ${INSTANCE} ${SOLUTION}`.

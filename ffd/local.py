import argparse
from collections import Counter
from common import CLUB_LIMIT, FIELD_NAMES, POSITIONS, Player, LIMIT, top11, get_players
import itertools as it

TOTAL_MONEY = 100.0


def read_solution(solution_file, players):
    squad = []
    ID_list = list(map(int, solution_file.readline().rstrip("\n").split(",")))
    ID_list += list(map(int, solution_file.readline().rstrip("\n").split(",")))
    return list(filter(lambda player: player.ID in ID_list, players))


def evaluate(player_combination, squad_club_counter, money_limit):
    combination_counter = Counter(map(lambda player: player.club, player_combination))
    combination_counter += squad_club_counter
    # .most_common() returns a list of tuples of form [(value, count)]
    most_common_value_count = combination_counter.most_common(1)[0][1]
    total_price = sum(player.price for player in player_combination)
    return total_price < money_limit and most_common_value_count <= 3


def local(players, squad):
    assert len(squad) == 15
    remaining_money = TOTAL_MONEY - sum(player.price for player in squad)
    assert remaining_money >= 0
    while True:
        # copy the squad list because it'll be modified
        squad_copy = squad.copy()
        better_players = []
        current_remaining_money = remaining_money
        for position in set(POSITIONS) - {"GK"}:
            worst_player_for_position = min(
                filter(lambda player: player.position == position, squad_copy),
                key=lambda player: player.points,
            )
            better_players_for_position = list(
                filter(
                    lambda player: player not in squad_copy
                    and player.position == position
                    and player.points >= worst_player_for_position.points,
                    players,
                )
            )
            assert len(better_players_for_position) >= 1
            better_players.append(better_players_for_position)
            squad_copy.remove(worst_player_for_position)
            current_remaining_money += worst_player_for_position.price
            # print(position, len(better_players_for_position))
        # print(current_remaining_money)
        club_counter = Counter(map(lambda player: player.club, squad_copy))
        # print(club_counter)
        possible_improvements = it.product(*better_players)
        possible_improvements_filtered = filter(
            lambda player_combination: evaluate(
                player_combination, club_counter, current_remaining_money
            ),
            possible_improvements,
        )
        try:
            better_players = max(
                possible_improvements_filtered,
                key=lambda combination: sum(player.points for player in combination),
            )
            squad_copy.extend(list(better_players))
            if set(squad_copy) != set(squad):
                squad = squad_copy
                remaining_money = 100 - sum(player.price for player in squad)
            else:
                return squad_copy
        except:
            return squad
    return squad


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--instance", required=True, type=argparse.FileType("r"))
    parser.add_argument(
        "-o",
        "--output",
        required=False,
        default="greedy.txt",
        type=argparse.FileType("w"),
    )
    parser.add_argument("-s", "--solution", required=True, type=argparse.FileType("r"))
    args = parser.parse_args()
    players = get_players(args.instance)
    squad = read_solution(args.solution, players)
    better_squad = local(players, squad)
    first_team, subs = top11(better_squad)
    args.output.write(",".join(map(lambda player: str(player.ID), first_team)) + "\n")
    args.output.write(",".join(map(lambda player: str(player.ID), subs)))
    return


if __name__ == "__main__":
    main()
